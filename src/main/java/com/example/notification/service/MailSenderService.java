package com.example.notification.service;

import com.example.notification.dto.OrderStatus;
import com.example.notification.dto.ProductDto;
import com.example.notification.dto.PurchaseDto;
import com.example.notification.dto.PurchaseProductDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.MailSendException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class MailSenderService {

    private static final String CHERTA = "\n------------------\n";
    private static final String CONTACT_DETAILS = "Contact us: +79112281337, gucc1godma1l@gmail.com";
    private static final String MESSAGE_NEW_ORDER = "Hello, %s!\nYour order %d is accepted and processing.\nOrder details:\n%s\n" + CONTACT_DETAILS;

    private final MailSender mailSender;

    public void sendOrderDetails(PurchaseDto purchase) {
        sendEmail(createMessage4OrderDetails(purchase));
    }

    public void sendOrderStatusUpd(OrderStatus status) {
        sendEmail(createMessage4OrderStatusUpd(status));
    }

    private void sendEmail(String text) {
        log.info("sendOrderDetails method: start...");
        SimpleMailMessage email = new SimpleMailMessage();
        email.setTo("gleb4a@gmail.com");
        email.setText(text);
        log.info("sendOrderDetails method: sending...");
        try {
            mailSender.send(email);
        } catch (MailSendException e) {
            log.error("sending error " + e.getMessage());
        }
        log.info("sendOrderDetails method: complete!");
    }

    private String createMessage4OrderDetails(PurchaseDto purchase) {
        StringBuilder stringBuilder = new StringBuilder();
        ProductDto product;
        int orderSum = 0;
        for (PurchaseProductDto purchProd : purchase.getPurchaseProducts()) {
            product = purchProd.getProduct();
            stringBuilder.append(product.getName()).append("\tx").append(purchProd.getQuantity())
                    .append("\n").append(product.getPrice()).append("$").append(CHERTA);
            orderSum += purchProd.getQuantity() * product.getPrice().intValue();
        }
        stringBuilder.append("Bill: ").append(orderSum).append("$\nOrder date: ")
                .append(purchase.getCheckoutDate()).append(CHERTA)
                .append(CONTACT_DETAILS);
        return String.format(MESSAGE_NEW_ORDER, purchase.getAccount().getName(), purchase.getId(), stringBuilder);
    }

    private String createMessage4OrderStatusUpd(OrderStatus status) {
        return "Your order is " + status.name().toLowerCase() + "!";
    }
}
