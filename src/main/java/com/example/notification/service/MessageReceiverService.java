package com.example.notification.service;

import com.example.notification.dto.OrderStatus;
import com.example.notification.dto.PurchaseDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class MessageReceiverService {

    private final MailSenderService mailSenderService;

    @RabbitListener(queues = "#{queue4newOrder.name}")
    public void receiveOrderDetails(PurchaseDto purchase) {
        log.info("Purchase received" + purchase);
        if (purchase == null) {
            log.error("MessageReceiverService: receiveOrderDetails: received null object");
            throw new NullPointerException("received null object");
        }
        log.info("Sending email...");
        mailSenderService.sendOrderDetails(purchase);
    }

    @RabbitListener(queues = "#{queue4orderUpd.name}")
    private void receiveOrderStatusUpd(OrderStatus status) {
        log.info("Message received::" + status);
        if (status == null) {
            log.error("MessageReceiverService: receiveOrderStatusUpd: received null object");
            throw new NullPointerException("received null object");
        }
        log.info("Sending email...");
        mailSenderService.sendOrderStatusUpd(status);
    }
}
