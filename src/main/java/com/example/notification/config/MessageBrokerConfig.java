package com.example.notification.config;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MessageBrokerConfig {

    private static final String ROUTING_KEY_STATUS = "purchase.update.*";
    private static final String ROUTING_KEY_NEW_ORDER = "purchase.new";

    @Value("${messagebroker.exchange.topic}")
    private String exchangeName;

    @Bean
    public Queue queue4newOrder() {
        return new AnonymousQueue();
    }

    @Bean
    public Queue queue4orderUpd() {
        return new AnonymousQueue();
    }

    @Bean
    public TopicExchange topicExchange() {
        return new TopicExchange(exchangeName);
    }

    @Bean
    public Binding bindingUpdateTopic(Queue queue4orderUpd, TopicExchange topicExchange) {
        return BindingBuilder.bind(queue4orderUpd).to(topicExchange).with(ROUTING_KEY_STATUS);
    }

    @Bean
    public Binding bindingNewOrderTopic(Queue queue4newOrder, TopicExchange topicExchange) {
        return BindingBuilder.bind(queue4newOrder).to(topicExchange).with(ROUTING_KEY_NEW_ORDER);
    }

    @Bean
    public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
        final var rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(producerJackson2MessageConverter());
        return rabbitTemplate;
    }

    @Bean
    public Jackson2JsonMessageConverter producerJackson2MessageConverter() {
        return new Jackson2JsonMessageConverter();
    }
}
