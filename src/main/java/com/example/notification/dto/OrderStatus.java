package com.example.notification.dto;

public enum OrderStatus {
    COMPLETED, DELIVERING, PROCESSING, DENIED
}
