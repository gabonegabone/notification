package com.example.notification.dto;

import com.example.notification.dto.ProductDto;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class PurchaseProductDto {

    private ProductDto product;
    private Integer quantity;
}
